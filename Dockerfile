FROM node:lts-slim

RUN apt-get update && \
    apt-get install --yes --no-install-recommends \
        git \
        ssh-client \
        ca-certificates \
        && \
    rm -rf /var/lib/apt/lists/* && \
    git version && \
    yarn global add @angular/cli@13.1.1
